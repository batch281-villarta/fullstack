// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import { Button, Form, Row, Col } from 'react-bootstrap';
//we need to import the useState from the react
import { useState, useEffect, useContext } from 'react';

import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import PageNotFound from './PageNotFound';

import Swal2 from 'sweetalert2';

export default function Register() {
  //State hooks to store the values of the input fields

  // 	{
  //     "firstName": "John",
  //     "lastName": "Smith",
  //     "email": "john@mail.com",
  //     "mobileNo": "09123456789",
  //     "password": "john1234"
  // }

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  //   const [password2, setPassword2] = useState('');

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  //   const [isPassed, setIsPassed] = useState(true);

  //   const [isDisabled, setIsDisabled] = useState(true);

  //we are going to add/create a state that will declare whether the password1 and password 2 is equal
  const [isPasswordMatch, setIsPasswordMatch] = useState(true);

  //when the email changes it will have a side effect that will console its value
  //   useEffect(() => {
  //     if (email.length > 15) {
  //       setIsPassed(false);
  //     } else {
  //       setIsPassed(true);
  //     }
  //   }, [email]);

  //this useEffect will disable or enable our sign up button
  //   useEffect(() => {
  //     //we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button.
  //     if (
  //       email !== '' &&
  //       password1 !== '' &&
  //     //   password2 !== '' &&
  //     //   password1 === password2 &&
  //       email.length <= 15
  //     ) {
  //       setIsDisabled(false);
  //     } else {
  //       setIsDisabled(true);
  //     }
  //   }, [email, password1, password2]);

  //function to simulate user registration
  function registerUser(event) {
    //prevent page reloading
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        mobileNo,
        password
      })
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === true) {
          Swal2.fire({
            title: 'Registration successful!',
            icon: 'success',
            text: 'Welcome to Zuitt!'
          });

          //   console.log(data);

          navigate('/login');
        } else if (data.message === 'Duplicate email found') {
          Swal2.fire({
            title: data.message,
            icon: 'error',
            text: 'Please provide a different email'
          });
        } else {
          Swal2.fire({
            title: 'Login unuccessful!',
            icon: 'error',
            text: 'Cannot register your account. Please try again!'
          });
        }
      });

    // alert('Thank you for registering!');
  }

  //useEffect to validate whether the password1 is equal to password2
  useEffect(() => {
    if (password !== password2) {
      setIsPasswordMatch(false);
    } else {
      setIsPasswordMatch(true);
    }
  }, [password, password2]);

  return user.id === null || user.id === undefined ? (
    <Row>
      <Col className="col-6 mx-auto">
        <h1 className="text-center">Register</h1>
        <Form onSubmit={(event) => registerUser(event)}>
          <Form.Group className="mb-3">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="First Name"
              value={firstName}
              required
              onChange={(event) => setFirstName(event.target.value)}
            />
            {/* <Form.Text className="text-danger">
              The email should not exceed 15 characters!
            </Form.Text> */}
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Last Name"
              value={lastName}
              required
              onChange={(event) => setLastName(event.target.value)}
            />
            {/* <Form.Text className="text-danger">
              The email should not exceed 15 characters!
            </Form.Text> */}
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              required
              onChange={(event) => setEmail(event.target.value)}
            />
            {/* <Form.Text className="text-danger">
              The email should not exceed 15 characters!
            </Form.Text> */}
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
              type="number"
              placeholder="Number"
              value={mobileNo}
              onChange={(event) => setMobileNo(event.target.value)}
            />
            {/* <Form.Text className="text-danger">
              The email should not exceed 15 characters!
            </Form.Text> */}
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              required
              onChange={(event) => setPassword(event.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password2}
              required
              onChange={(event) => setPassword2(event.target.value)}
            />
            <Form.Text className="text-danger" hidden={isPasswordMatch}>
              The passwords does not match!
            </Form.Text>
          </Form.Group>

          {/* <Form.Group className="mb-3" controlId="formBasicPassword2">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Retype your nominated password"
              value={password2}
              onChange={(event) => setPassword2(event.target.value)}
            />

            <Form.Text className="text-danger" hidden={isPasswordMatch}>
              The passwords does not match!
            </Form.Text>
          </Form.Group> */}

          <Button variant="primary" type="submit">
            Sign up
          </Button>
        </Form>
      </Col>
    </Row>
  ) : (
    <PageNotFound />
  );
}
