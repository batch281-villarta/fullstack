let collection = [];

// Write the queue functions below.

function print() {
  // It will show the array
  return collection;
}

function enqueue(element) {
  //In this function you are going to make an algo that will add an element to the array (last element)
  collection.length + 1;
  collection[collection.length] = element;

  return collection;
}

function dequeue() {
  // In here you are going to remove the first element in the array

  for (let i = 0; i < collection.length - 1; i++) {
    collection[i] = collection[i + 1];
  }

  collection.length = collection.length - 1;

  return [collection[0]];
}

function front() {
  // you will get the first element
  return collection[0];
}

function size() {
  // Number of elements

  let sizeCounter = 0;
  // collection.forEach((x) => sizeCounter++);

  for (let i = 0; i < collection.length; i++) {
    sizeCounter++;
  }

  return sizeCounter;
}

function isEmpty() {
  return size() ? false : true;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};
